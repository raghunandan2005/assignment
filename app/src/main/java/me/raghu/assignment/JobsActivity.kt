package me.raghu.assignment

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import me.raghu.assignment.models.Jobs
import me.raghu.assignment.utils.JobsResult
import me.raghu.assignment.utils.JobsResult.Success
import android.util.Log
import java.lang.Exception


class JobsActivity : AppCompatActivity() {

    private lateinit var mAdapter: JobsAdapter
    private lateinit var locationViewModel: LocationViewModel
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var isLoading = false
    private var currentPage = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mAdapter = JobsAdapter(this@JobsActivity)
        recyclerView.setHasFixedSize(true)
        recyclerView.addItemDecoration(DividerItemDecoration(this@JobsActivity, 1))
        linearLayoutManager = LinearLayoutManager(this@JobsActivity)
        recyclerView.layoutManager = linearLayoutManager
        recyclerView.adapter = mAdapter

        recyclerView.addOnScrollListener(object : EndLessScrollListener(currentPage) {
            override fun onLoadMore(current_page: Int, totalItemCount: Int) {
                Log.i("Count", "" + current_page)
                    loadMoreItems(current_page)
            }
        })


        locationViewModel = ViewModelProviders.of(this).get(LocationViewModel::class.java)
        locationViewModel.fetchData(currentPage)
        locationViewModel.getJobs().observe(this, Observer {
            when (it) {

                JobsResult.ShowLoader -> showProgressBar()
                is Success -> showData(it.jobs)
                is JobsResult.Error -> showError(it.error)
                is JobsResult.NetworkException -> noNetwork(it.cause)
        }})
    }

    private fun noNetwork(exception: Exception){
        Log.e("Exception", "" + exception.printStackTrace())
        if(isLoading){
            isLoading = false
            recyclerView.post {
                mAdapter.removeItem()
            }
        }else {

            dismiss()
            errorText.visibility = View.VISIBLE
            errorText.text = "No network Connection"
            recyclerView.visibility = View.GONE
        }

    }

   private fun loadMoreItems(currentPage: Int) {
        isLoading = true
        locationViewModel.fetchData(currentPage)
        recyclerView.post {
            mAdapter.addItem()
        }

    }

    private fun showError(error: String) {
        if (isLoading) {
            isLoading = false
            recyclerView.post {
                mAdapter.removeItem()
            }

        } else {
            dismiss()
            errorText.visibility = View.VISIBLE
            errorText.text = error
            recyclerView.visibility = View.GONE
        }
    }

    private fun showProgressBar() {
        if (!isLoading) {
            progressBar.visibility = View.VISIBLE
            errorText.visibility = View.GONE
            recyclerView.visibility = View.GONE
        }
    }

    private fun showData(jobs: List<Jobs>) {
        if (isLoading) {
            isLoading = false
            recyclerView.post {
                mAdapter.removeItem()
                mAdapter.addItems(jobs)
            }
        } else {
            dismiss()
            errorText.visibility = View.GONE
            recyclerView.visibility = View.VISIBLE
            mAdapter.addItems(jobs)
        }

    }

    private fun dismiss() {
        progressBar.visibility = View.GONE
    }
}

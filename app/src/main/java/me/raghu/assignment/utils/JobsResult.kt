package me.raghu.assignment.utils


import me.raghu.assignment.models.Jobs

sealed class JobsResult {
    data class Success(val jobs: List<Jobs>) : JobsResult()

    //object ConnectionError : JobsResult()
    object ShowLoader : JobsResult()

    data class Error(val error: String) : JobsResult()
    //data class Failure(val message: String) : JobsResult()
    //data class UnexpectedError(val message: String) : JobsResult()
    data class NetworkException(val cause: Exception) : JobsResult()

}

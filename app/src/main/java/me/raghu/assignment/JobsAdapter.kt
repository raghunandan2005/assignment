package me.raghu.assignment

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.job_row.view.*
import me.raghu.assignment.downloadimage.ImageLoader
import me.raghu.assignment.models.Jobs
import java.util.*


class JobsAdapter(
    private val context: Context,
    private val items: ArrayList<Jobs?> = ArrayList()
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    init {
        setHasStableIds(true)
    }

    companion object {
        const val TYPE_DATA = 0
        const val TYPE_PROGRESS = 1
    }

    private inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var progressBar: ProgressBar = itemView.findViewById(R.id.progressBar)

    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position] == null) TYPE_PROGRESS else TYPE_DATA
    }

    fun addItems(jobs: List<Jobs>) {
        items.addAll(jobs)
        Log.i("Items size", "" + items.size)
        notifyItemRangeInserted(itemCount, jobs.size)
    }

    fun removeItem() {
        Log.i("Items size", "" + items.size)
        val removeAt = items.removeAt(items.size - 1)
        Log.i("Items size", "" + removeAt)
        notifyDataSetChanged()
    }

    fun addItem() {
        items.add(null)
        Log.i("Items size", "" + items.size)
        notifyItemInserted(items.size - 1)
    }


    override fun getItemCount(): Int = items.size


    override fun getItemId(position: Int): Long =
        items[position]?.title.hashCode().toLong()


    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return if (viewType == TYPE_DATA) {

            ViewHolder(LayoutInflater.from(context).inflate(R.layout.job_row, parent, false))
        } else {
            val view =
                LayoutInflater.from(parent.context).inflate(R.layout.progress_below, parent, false)
            LoadingViewHolder(view)
        }


    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
     /*   if(holder is ViewHolder){
            val future:Future<*> = holder.itemView.tag as Future<*>
            future.cancel(true)
        }*/
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder.itemViewType) {
            TYPE_DATA -> {
                holder as ViewHolder
                items[position]?.let {
                    holder.bind(it)
                }
            }
            TYPE_PROGRESS -> {
                holder as LoadingViewHolder

            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        fun bind(jobs: Jobs) {

            itemView.title.text = jobs.title
            itemView.descrp.text = jobs.description
            if (jobs.company_logo != null) {
                // initially set the placeholder
                // view may be recycled
                // to avoid displaying wrong image at position set the place holder
                // when image is downloaded or retrieved from cache display proper image
                // disk cache to be added.
                itemView.imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        itemView.imageView.context,
                        R.drawable.img_placeholder))
                ImageLoader.with(context).load(itemView.imageView, jobs.company_logo)
            }else{
                itemView.imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                    itemView.imageView.context,
                    R.drawable.img_placeholder))
            }

            // Downloading Image without glide is possible
            // But requires lot of effort which does not fit the
            // scope of assignment
          /* if (jobs.company_logo != null) {

                Glide
                    .with(itemView.context)
                    .load(jobs.company_logo)
                    .placeholder(R.drawable.img_placeholder)
                    .error(R.mipmap.ic_launcher)
                    .into(itemView.imageView)


            } else {
                Glide
                    .with(itemView.context).clear(itemView.imageView)
                itemView.imageView.setImageDrawable(
                    ContextCompat.getDrawable(
                        itemView.imageView.context,
                        R.drawable.img_placeholder
                    )
                )

            }*/
        }
    }
}

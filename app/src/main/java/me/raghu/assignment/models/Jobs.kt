package me.raghu.assignment.models

import android.os.Parcel
import android.os.Parcelable

data class Jobs(
    val company: String?,
    val company_logo: String?,
    val company_url: String?,
    val created_at: String?,
    val description: String?,
    val how_to_apply: String?,
    val id: String?,
    val location: String?,
    val title: String?,
    val type: String?,
    val url: String?
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString(),
        source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(company)
        writeString(company_logo)
        writeString(company_url)
        writeString(created_at)
        writeString(description)
        writeString(how_to_apply)
        writeString(id)
        writeString(location)
        writeString(title)
        writeString(type)
        writeString(url)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Jobs> = object : Parcelable.Creator<Jobs> {
            override fun createFromParcel(source: Parcel): Jobs = Jobs(source)
            override fun newArray(size: Int): Array<Jobs?> = arrayOfNulls(size)
        }
    }
}
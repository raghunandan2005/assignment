package me.raghu.assignment

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import me.raghu.assignment.models.Jobs
import me.raghu.assignment.utils.JobsResult
import java.io.*
import java.net.HttpURLConnection
import java.net.URL


class Repository {

    private var jobsMutableLiveData: MutableLiveData<JobsResult> = MutableLiveData()

    fun getData(pageNumber:Int): MutableLiveData<JobsResult> {

        // for the sake of example getting data and parsing is put here
        // ideally you would move this to a separate class
        Thread(Runnable {
            var jobsResult:JobsResult
            jobsResult = JobsResult.ShowLoader
            jobsMutableLiveData.postValue(jobsResult)
            var urlConnection: HttpURLConnection? = null
            //val result = StringBuilder()
            val urlBuilder = StringBuilder()
            urlBuilder.append("https://jobs.github.com/positions.json?page=")
            urlBuilder.append(pageNumber)
            Log.i("Url",""+urlBuilder.toString())
            try {
                val url =
                    URL(urlBuilder.toString())
                urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.setConnectTimeout(5000);
                urlConnection.setReadTimeout(5000);

                when(val code = urlConnection.responseCode) {
                    200 -> {
                        val inputStream = BufferedInputStream(urlConnection.inputStream)

                        val response = convertInputStreamToString(inputStream)

                        val gson = Gson()
                        val jobs = gson.fromJson(response, arrayOf<Jobs>()::class.java)
                        if(jobs!=null) {
                            jobsResult = if (jobs.isNotEmpty()) {
                                JobsResult.Success(jobs.toList())
                            } else {
                                JobsResult.Error("No jobs to display")
                            }
                        } else{
                            JobsResult.Error("No Data to dispaly")
                        }

                    }
                    408 -> jobsResult = JobsResult.Error("Cannot make connection!")
                    // ideally get error stream and extract error message
                    500 -> jobsResult = JobsResult.Error("Something wrong on the server side")
                    else -> jobsResult = JobsResult.Error(code.toString())
                }

            } catch (e: Exception) {
                jobsResult = JobsResult.NetworkException(e)
            } finally {
                urlConnection?.disconnect()
            }
            jobsMutableLiveData.postValue(jobsResult)
        }).start()
        return jobsMutableLiveData
    }

    private fun convertInputStreamToString(inputStream: InputStream): String {
        val bufferedReader = BufferedReader(InputStreamReader(inputStream))
        val sb = StringBuilder()
        var line: String? = null
        try {
            while ({ line = bufferedReader.readLine(); line }() != null)  {
                sb.append(line)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return sb.toString()
    }
}
package me.raghu.assignment

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import me.raghu.assignment.utils.JobsResult


class LocationViewModel : ViewModel() {

    private val repository = Repository()

    private val jobs: MediatorLiveData<JobsResult> = MediatorLiveData()

    fun fetchData(currentPage: Int) {

        jobs.addSource(repository.getData(currentPage), statusObserver)
    }

    private var statusObserver = Observer<JobsResult> {
        jobs.value = it
    }

    fun getJobs(): MediatorLiveData<JobsResult> {
        return jobs
    }
}